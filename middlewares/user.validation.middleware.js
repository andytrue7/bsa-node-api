import { USER } from "../models/user.js";
import { userService } from "../services/userService.js";

const emailRegExp = /^[a-zA-Z0-9]*@gmail\.com$/;
const phoneNumberRegExp = /^\+380[0-9]{9}/;

const checkExtraFields = (requestBody, model) => {
  const propertyArr = Object.getOwnPropertyNames(requestBody);
  return propertyArr.every(((item) => model.hasOwnProperty(item)));
};

const createUserValid = (req, res, next) => {
  const {id, ...requestBody} = req.body;
  if (checkReqBodyFieldsWhenCreate(requestBody)) {
    next();        
  } else {
    res.status(400).send({ error: true, message: 'User data is not valid' });
  }
};

const updateUserValid = (req, res, next) => {
  const {id, ...requestBody} = req.body;
  const user = userService.search({id: req.params.id});
  
  if (user && checkReqBodyFieldsWhenUpdate(requestBody)) {
    next();        
  } else {
    res.status(400).send({ error: true, message: 'User data is not valid' });
  }
};

function checkReqBodyFieldsWhenCreate(requestBody) {
  return checkExtraFields(requestBody, USER) &&
    requestBody.firstName &&
    requestBody.lastName &&
    requestBody.phoneNumber &&
    requestBody.phoneNumber.match(phoneNumberRegExp) &&
    requestBody.password &&
    requestBody.password.length >= 3 &&
    requestBody.email &&
    requestBody.email.match(emailRegExp)
}

function checkReqBodyFieldsWhenUpdate(requestBody) {
  if (!Object.keys(requestBody).length) {
    return false;
  }

  if (!checkExtraFields(requestBody, USER)) {
    return false;
  }
  
  if (requestBody.phoneNumber && !requestBody.phoneNumber.match(phoneNumberRegExp)) {
    return false;
  }

  if (requestBody.password && !(requestBody.password.length >= 3)) {
    return false;
  }

  if (requestBody.email && !requestBody.email.match(emailRegExp)) {
    return false;
  }

  return true;
}

export { createUserValid, updateUserValid };
