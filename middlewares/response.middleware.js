const responseMiddleware = (req, res, next) => {
   const data = res.locals.responseData;
   const errorText = res.locals.errorText;

   if (data) {
      res.status(200).send(data);   
   } else {
      res.status(404).send({ error: true, message: errorText });
   }

  next();
};

export { responseMiddleware };
