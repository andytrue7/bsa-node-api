import { FIGHTER } from "../models/fighter.js";
import { fighterService } from "../services/fighterService.js";

const checkExtraFields = (requestBody, model) => {
  const propertyArr = Object.getOwnPropertyNames(requestBody);
  return propertyArr.every(((item) => model.hasOwnProperty(item)));
};

const createFighterValid = (req, res, next) => {
  const {id, ...requestBody} = req.body;
  if (checkReqBodyFieldsWhenCreate(requestBody)) {
    next();   
  } else {
    res.status(400).send({ error: true, message: 'Fighter data is not valid' });
  }
};

const updateFighterValid = (req, res, next) => {
  const {id, ...requestBody} = req.body;
  const user = fighterService.search({id: req.params.id});
  
  if (user && checkReqBodyFieldsWhenUpdate(requestBody)) {
    next();   
  } else {
    res.status(400).send({ error: true, message: 'Fighter data is not valid' });
  }
};

function checkReqBodyFieldsWhenCreate(requestBody) {
  const isRequireFieldsValid = requestBody.name
  && requestBody.power
  && requestBody.power >= 1
  && requestBody.power <= 100
  && requestBody.defense 
  && requestBody.defense >= 1
  && requestBody.defense <= 10
  &&checkExtraFields(requestBody, FIGHTER)

  if (!isRequireFieldsValid) {
    return false;
  }
  
  if (requestBody.health < 80 || requestBody.health > 120) {
    return false;
  }
  
  return true;
}

function checkReqBodyFieldsWhenUpdate(requestBody) {
  if (!Object.keys(requestBody).length) {
    return false;
  }

  if (!checkExtraFields(requestBody, FIGHTER)) {
    return false;
  }
  
  const isPowerNotValid = requestBody.power < 1 || requestBody.power > 100;
  if (isPowerNotValid) {
    return false;
  }

  const isDefenseNotValid = requestBody.defense < 1 || requestBody.defense > 10;
  if (isDefenseNotValid) {
    return false;
  }

  const isHealthNotValid = requestBody.health < 80 || requestBody.health > 120;
  if (isHealthNotValid) {
    return false;
  }

  return true;
}

export { createFighterValid, updateFighterValid };
