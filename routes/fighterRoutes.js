import { Router } from "express";
import { fighterService } from "../services/fighterService.js";
import { responseMiddleware } from "../middlewares/response.middleware.js";
import {
  createFighterValid,
  updateFighterValid,
} from "../middlewares/fighter.validation.middleware.js";

const router = Router();

router.get('/', (req, res, next) => {
  const fighters = fighterService.getAllFighters();
  res.locals.responseData = fighters.data;
  res.locals.errorText = fighters.errorText;
  next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  const {id, ...requestBody} = req.body;
  const fighter = fighterService.saveFighter(requestBody);
  res.locals.responseData = fighter.data;
  res.locals.errorText = fighter.errorText;
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  const fighter = fighterService.search({ id });
  res.locals.responseData = fighter.data;
  res.locals.errorText = fighter.errorText;
  next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  const id = req.params.id;
  const {id: reqBodyId, ...requestBody} = req.body;
  const fighter = fighterService.updateFighter(id, requestBody);
  res.locals.responseData = fighter.data;
  res.locals.errorText = fighter.errorText;
  next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  const fighter = fighterService.deleteFighter(id);
  res.locals.responseData = fighter.data;
  res.locals.errorText = fighter.errorText;
  next();
}, responseMiddleware);

export { router };
