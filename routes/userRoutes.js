import { Router } from "express";
import { userService } from "../services/userService.js";
import {
  createUserValid,
  updateUserValid,
} from "../middlewares/user.validation.middleware.js";
import { responseMiddleware } from "../middlewares/response.middleware.js";

const router = Router();

router.get('/', (req, res, next) => {
  const users = userService.getAllUsers();
  res.locals.responseData = users.data;
  res.locals.errorText = users.errorText;
  next();
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  const {id, ...requestBody} = req.body;
  const user = userService.saveUser(requestBody);
  res.locals.responseData = user.data;
  res.locals.errorText = user.errorText;
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  const user = userService.search({ id });
  res.locals.responseData = user.data;
  res.locals.errorText = user.errorText;
  next();
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  const id = req.params.id;
  const {id: reqBodyId, ...requestBody} = req.body;
  const user = userService.updateUser(id, requestBody);
  res.locals.responseData = user.data;
  res.locals.errorText = user.errorText;
  next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  const user = userService.deleteUser(id);
  res.locals.responseData = user.data;
  res.locals.errorText = user.errorText;
  next();
}, responseMiddleware);

export { router };
