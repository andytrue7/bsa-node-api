import { FIGHTER } from "../models/fighter.js";
import { fighterRepository } from "../repositories/fighterRepository.js";

class FighterService {
  getAllFighters() {
    const fighters = fighterRepository.getAll();
    if (!fighters) {
      return {
        data: null,
        errorText: 'Something went wrong'
      };
    }
    return { data: fighters,  errorText: null};
  }

  search(search) {
    const fighter = fighterRepository.getOne(search);
    if(!fighter) {
      return {
        data: null,
        errorText: 'Fighter not found'
      };
    }
    return {
      data: fighter,
      errorText: null
    };
  }
  
  saveFighter(data) {
    const existeFighterByName = this.search({ name: data.name });
    if (existeFighterByName) {
      return {
        data: null,
        errorText: 'Fighter already exists by this name'
      };
    }
    
    const fighter = fighterRepository.create(data);
    if (!fighter) {
        return {
          data: null,
          errorText: 'Fighter not created'
        };
    }

    if (!fighter.health) {
      fighter.health = FIGHTER.health;
    }

    return {
      data: fighter,
      errorText: null
    };;
  }

  updateFighter(id, data) {
    const fighter = fighterRepository.update(id, data);
    if (!fighter) {
        return {
        data: null,
        errorText: 'FIghter not updated'
      };
    }
    return {
      data: fighter,
      errorText: null
    };
  }

  deleteFighter(id) {
    const fighter = fighterRepository.delete(id);
    if (!fighter) {
        return {
          data: null,
          errorText: 'FIghter not deleted'
        };
    }
    return {
      data: fighter,
      errorText: null
    };
  }

}

const fighterService = new FighterService();

export { fighterService };
