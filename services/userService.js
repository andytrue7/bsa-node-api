import { userRepository } from "../repositories/userRepository.js";

class UserService {

  getAllUsers() {
    const users = userRepository.getAll();
    if (!users) {
      return {
        data: null,
        errorText: 'Something went wrong'
      };
    }
    return { data: users,  errorText: null}; 
  }

  search(search) {
    const user = userRepository.getOne(search);
    if (!user) {
      return {
        data: null,
        errorText: 'User not found'
      };
    }
    return {
      data: user,
      errorText: null
    };
  }

  saveUser(data) {
    const existedUserByEmail = this.search({ email: data.email });
    if (existedUserByEmail) {
      return {
        data: null,
        errorText: 'User already exists by this email'
      };
    }
    
    const existedUserByPhone = this.search({ phoneNumber: data.phoneNumber });
    if (existedUserByPhone) {
      return {
        data: null,
        errorText: 'User already exists by this phone number'
      };
    }

    const user = userRepository.create(data);
    if (!user) {
      return {
        data: null,
        errorText: 'User is not created'
      };
    }
    return {
      data: user,
      errorText: null
    };
  }

  updateUser(id, data) {
    const user = userRepository.update(id, data);
    if (!user) {
      return {
        data: null,
        errorText: 'User not updated'
      };
    }
    return {
      data: user,
      errorText: null
    };
  }

  deleteUser(id) {
    const user = userRepository.delete(id);
    if (!user) {
      return {
        data: null,
        errorText: 'User not deleted'
      };
    }
    return {
      data: user,
      errorText: null
    };
  }
}

const userService = new UserService();

export { userService };
